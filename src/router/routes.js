//const Home = () => import('@/pages/Home')
//使用路由懒加载，函数体在component中
const Detail = () => import('@/pages/Detail')
const Login = () => import('@/pages/Login')
const Register = () => import('@/pages/Register')
const Search = () => import('@/pages/Search')   
const AddCartSuccess = () => import('@/pages/AddCartSuccess')
const ShopCart = () => import('@/pages/ShopCart')
const Trade = () => import('@/pages/Trade')
const Pay = () => import('@/pages/Pay')
const PaySuccess = () => import('@/pages/PaySuccess')
const Center = () => import('@/pages/Center')
const Myorder = () => import('@/pages/Center/Myorder')
export default [{
        path: '',
        redirect: '/home'
    },
    {
        path: '/home',
        component: () => import('@/pages/Home'),
        meta: {
            show: true
        }
    },
    //添加购物车成功页面
    {
        path: '/addcartsuccess',
        component: AddCartSuccess,
        name:'addcartsuccess',
        meta: {
            show: true
        }
    },
    //购物车界面
    {
        path: '/shopcart',
        component: ShopCart,
        name:'shopcart',
        meta: {
            show: true
        }
    },
    //商品详情页代码、路由信息
    {
        path: '/detail/:skuid',
        component: Detail,
        meta: {
            show: true
        }
    },
    {
        path: '/login',
        component: Login,
        meta: {
            show: false
        }
    },
    {
        path: '/register',
        component: Register,
        meta: {
            show: false
        }
    },
    {
        path: '/search/:keyword?',
        component: Search,
        name: 'search',
        meta: {
            show: true
        },
    },
    //购物车支付界面
    {
        path: '/trade',
        component: Trade,
        meta: {
            show: true
        },
        beforeEnter: (to, from, next) => {
            //只能从shopcart来
            if(from.path == '/shopcart'){
                next()
            }else{
                next(from.path)
            }
          }
    },
    //订单-支付（pay）界面
    //购物车支付界面
    {
        path: '/pay',
        component: Pay,
        meta: {
            show: true
        },
        beforeEnter: (to, from, next) => {
            //只能从trade来
            if(from.path == '/trade'){
                next()
            }else{
                next(from.path)
            }
          }
    },
    //支付成功界面
    {
        path: '/paysuccess',
        component: PaySuccess,
        meta: {
            show: true
        },
        beforeEnter: (to, from, next) => {
            //只能从pay来
            if(from.path == '/pay'){
                next()
            }else{
                next(from.path)
            }
          }
    },
    //个人订单 // 个人中心界面
    {
        path: '/center',
        component: Center,
        meta: {
            show: true
        },
        children:[{
            //写全路由地址或者 省略掉center
            path: '/center/myorder',
            component: Myorder,
        },
        //二级路由重定向
        {
            path: '/center',
            redirect:'/center/myorder'
        }
    ]
    },
]