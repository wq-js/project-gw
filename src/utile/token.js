//持续本地保留token
export const setToken=(token) =>{
    localStorage.setItem('TOKEN',token)
};
//读取本地的token
export const getToken = () =>{
    return localStorage.getItem('TOKEN')
};
export const tokenRemove = () =>{
    localStorage.removeItem('TOKEN')
}