import { v4 as uuidv4 } from 'uuid';
export const getUUID=() =>{
    let uuid_taken = localStorage.getItem('UUIDTAKEN');
    if(!uuid_taken){
        uuid_taken = uuidv4();
        localStorage.setItem('UUIDTAKEN',uuid_taken);
    }
    return uuid_taken;
}