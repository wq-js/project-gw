//引入mockjs
import Mock from 'mockjs';
import banner from './banner.json';
import floor from './floor.json';

//Mock数据：默认要两个参数 ，第一个：参数请求地址，第二个：请求数据
Mock.mock('/mock/banner',{code:200,data:banner});
Mock.mock('/mock/floor',{code:200,data:floor});