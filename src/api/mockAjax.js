import axios from 'axios';
//引用进度条
import nprogress from 'nprogress';
//导入进度条样式
import "nprogress/nprogress.css"
//创建axios实例，设置 基础网络请求
const requests = axios.create({
    baseURL:'/mock',
    timeout:5000
})
//请求拦截器：可在发送请求前，做一些事情
requests.interceptors.request.use((config)=>{
    //config；配置对象，里面有一个重要的属性：headers（请求头）
    nprogress.start();
    return config
});
//响应拦截器：分别对成功、失败进行处理
requests.interceptors.response.use((res)=>{
    nprogress.done();
    return res.data
},(error)=>{
    return Promise.reject(new Error("faile"))
});
//对外暴露，
export default requests;