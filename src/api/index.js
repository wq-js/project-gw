import requests from "./request";
import mockreq from './mockAjax';
//三级列表联动，封装成函数
export const reqCategoryList =()=>requests({url:'/product/getBaseCategoryList',method:'get'});
//获取banner(/home)界面轮播图数据
export const reqBannerList =()=>mockreq({url:'/banner',method:'get'});
//获取floor数据
export const reqFloorList =()=>mockreq({url:'/floor',method:'get'});
//获取search数据
//使用axios 对象格式写法，带有参数
export const reqGetSearchList = (params) =>requests({
    url:'/list',
    method:'post',
    data:params,
})
//获取商品详情页数据
export const reqGetgoodInfo = (skuId) =>requests({
    url:`/item/${skuId}`,
    method:'get',
})
//给服务器发送购物车信息
export const reqAddorUpdateShopCart = (skuId,skuNum) =>requests({
    url:`/cart/addToCart/${ skuId }/${ skuNum }`,
    method:'post',
})
//获取个人购物车数据
export const reqUserShopCart = () =>requests({
    url:'/cart/cartList',
    method:'get',
})
//删除购物车数据
export const reqDeleteShopCartInfo = (skuId) =>requests({
    url:`/cart/deleteCart/${skuId}`,
    method:'DELETE',
})
//修改购物车选中状态
export const reqChangeShopCheckedInfo = (skuId,isChecked) =>requests({
    url:`/cart/checkCart/${skuId}/${isChecked}`,
    method:'GET',
})
//获取用户验证码请求--用于注册等页面
export const reqGetUserCode = (phone) =>requests({
    url:`/user/passport/sendCode/${phone}`,
    method:'get',
})
//用户注册接口  phone、code、password
export const reqUserRegister = (data) =>requests({
    url:'/user/passport/register',
    data,
    method:'post'
})
//用户登录且验证
//用户注册接口  phone、password
export const reqUserLogin = (data) =>requests({
    url:'/user/passport/login',
    data,
    method:'post'
})
//验证token正确性，以及返回用户信息
export const reqUserInfo =() =>requests({
    url:'/user/passport/auth/getUserInfo',
    method:'get'
})
//退出登录接口，无参数
export const reqUserLogout =() =>requests({
    url:'/user/passport/logout',
    method:'get'
})
//结算界面中用户地址数据
export const reqUserAddress =() =>requests({
    url:'/user/userAddress/auth/findUserAddressList',
    method:'get'
})
//获取结算界面中用户商品信息
export const reqOrderInfo =() =>requests({
    url:'/order/auth/trade',
    method:'get'
})
//提交订单
export const submitOrder =(tradeNo,data)=>requests({
    url:`/order/auth/submitOrder?tradeNo=${tradeNo}`,
    data,
    method:'post'
})
//获取订单支付信息
export const orderInfo =(orderId)=>requests({
    url:`/payment/weixin/createNative/${orderId}`,
    method:'get'
})
//查询订单支付状态 --已支付 支付中等
export const orderStatus = (orderId)=>requests({
    url:`/payment/weixin/queryPayStatus/${orderId}`,
    method:'get'
})
//查询订单支付状态 --已支付 支付中等
export const getMyOrder = (page,limit)=>requests({
    url:`/order/auth/${page}/${limit}`,
    method:'get'
})