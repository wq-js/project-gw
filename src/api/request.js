import axios from 'axios';
//引用进度条
import nprogress from 'nprogress';
//获取uuid，得到购物车信息
import store from '@/store';
//导入进度条样式
import "nprogress/nprogress.css"
//创建axios实例，设置 基础网络请求
const requests = axios.create({
    baseURL:'/api',
    timeout:5000
})
//请求拦截器：可在发送请求前，做一些事情
requests.interceptors.request.use((config)=>{
    //config；配置对象，里面有一个重要的属性：headers（请求头）
    if(store.state.detail.uuid_taken){
        config.headers.userTempId = store.state.detail.uuid_taken;
    };
    if(store.state.user.token){
        config.headers.token = store.state.user.token;
    };
    nprogress.start();
    return config
});
//响应拦截器：分别对成功、失败进行处理
requests.interceptors.response.use((res)=>{
    nprogress.done();
    return res.data
},(error)=>{
    return Promise.reject(new Error("faile"))
});
//对外暴露，
export default requests;