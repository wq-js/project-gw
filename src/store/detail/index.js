import { reqGetgoodInfo,reqAddorUpdateShopCart } from "@/api/index";
import {getUUID} from '@/utile/uuid_taken.js'
const state = {
    goodInfo :{},
    uuid_taken: getUUID()
};
const mutations ={
    GETGOODINFO(state,goodInfo){
        state.goodInfo = goodInfo;
    }
};
const actions = {
    async getGoodInfo({commit},skuId){
        let result = await reqGetgoodInfo(skuId)
        if (result.code == 200){
            commit('GETGOODINFO',result.data)
        }

    },
    async AddorUpdateShopCartInfo({commit},{skuId,skuNum}){
        let result = await reqAddorUpdateShopCart(skuId,skuNum)
        if(result.code == 200){
            return 'ok'
        }else{
            return Promise.reject(new Error('添加购物车失败'))
        }
     },
};
const getters = {
    categoryView(state){
        return state.goodInfo.categoryView||{};
    },
    skuInfo(state){
        return state.goodInfo.skuInfo||{};
    },
    spuSaleAttrList(state){
        return state.goodInfo.spuSaleAttrList ||[];
    }
};
export default {
    state,
    mutations,
    actions,
    getters
}