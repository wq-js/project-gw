import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
//注册 三级联动全局组件
import TypeNav from '@/components/TypeNav/TypeNav.vue'
//引入 轮播图全局组件
import CarouSel from '@/components/Carousel/index.vue'
//引入分页器组件
import Pagination from '@/components/Pagination/index.vue'
import {MessageBox,Button} from 'element-ui'
//按需引入element ui 组件 
Vue.component(Button.name,Button);
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
//注册全剧组件 第一个参数：注册组件的名字，第二个参数：需要注册哪个组件
Vue.component(TypeNav.name,TypeNav)
Vue.component(CarouSel.name,CarouSel)
Vue.component(Pagination.name,Pagination)
//引入MockServe文件
import "@/mock/mockServe";
//引入swiper样式
import 'swiper/css/swiper.css';
//引入请求网址，挂载到vue原型链上
import * as API from '@/api/index';
import lazy from '@/assets/lazy.gif'
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload,{
  loading:lazy
})
new Vue({
  render: h => h(App),
  beforeCreate(){
  Vue.prototype.$bus = this;
  Vue.prototype.$API = API;
  },
  router,
  store
}).$mount('#app')
